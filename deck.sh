#!/bin/bash

file=$(basename -s .sh $0)

# export as `bash deck-bash-array.sh pdf` and it will generate deck-bash-array.pdf
# requires enscript and ps2pdf to be installed
if [ "$1" == "pdf" ]; then
    echo "generate pdf"
    TMPDIR=$(mktemp -d)
    echo "TMPDIR=$TMPDIR"
    len=${#slide[*]}
    pages=""
    n=0
    while [ $n -lt $len ]
    do
        echo "${slide[$n]}

[$(($n+1))/${#slide[*]}]" > "$TMPDIR/page$n.txt"
        pages="$pages $TMPDIR/page$n.txt"
        n=$((n+1))
    done
    enscript --no-header -p "$file.ps" $pages
    ps2pdf "$file.ps"
    rm "$file.ps"
    rm -rf "$TMPDIR"
    exit 0
fi

n=0
while :
do
    clear
    echo "${slide[$n]}

[$(($n+1))/${#slide[*]}]"
    read cmd
    case $cmd in
        "") n=$((n+1)); ;;
	[-] ) n=$((n-1)); ;;
	$ ) n=${#slide[*]}; ;;
        [0-9]* ) n=$((cmd-1)); ;;
	[q] ) exit; ;;
    esac
    if [ $n -gt $((${#slide[*]}-1)) ]; then n=$((${#slide[*]}-1)); fi
    if [ $n -lt 0 ]; then n=0; fi
done
