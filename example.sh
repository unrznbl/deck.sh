slide=(
"`figlet deck.sh Example`

Welcome to the example slide deck for deck.sh
"

"This is just a super easy way to create
a slide deck presentation and navigate it
ed(1) style as well as export it as a PDF
for posting to the internet.
"

"Usage:

create a shell script with a single "slide" array
and source deck.sh and run the slide deck script

bash example.sh
"

"To export to PDF you will need to install:

- enscript and ps2pdf

Run:

bash example.sh pdf
and this produces example.pdf
"
)
source deck.sh
