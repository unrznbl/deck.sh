# deck.sh

a slide deck player and exporter (to pdf) written in bash and using a bash array as data

run the deck with

`bash example.sh`

controls are ed(1) like

<n> - go to slide number n
<enter> - next slide
"-" - previous slide
$ - last slide
q - quit

export to pdf with

`bash example.sh pdf`